// Problem: We need a simple way to look at a user's badge count and JavaScript points
// Solution: Use Node.js to connect to Treehouse's API to get profile information to print out

//Require Http module
const https = require('https');
const currency = 'USD';
https: //www.doviz.com/api/v1/currencies/USD/latest
function currencyMessage(Selling, buying, fullName, Code) {
    const message = `${fullName}- ${Code} Buying Price: ${buying} and Selling Price ${Selling}`;
    console.log(message);

}

function getCurrencyRate(currencyName){
    const request = https.get(`https://www.doviz.com/api/v1/currencies/${currencyName}/latest`, response => {
        // Read the data
     //   console.log(response.statusCode);
        let body = "";
        response.on('data', data => {
            body += data.toString();
        });
        
        response.on('end', () => {
            // Parse the data
            const kur = JSON.parse(body);
          //  console.dir(kur);

          //  console.log(kur.selling, kur.buying, kur.full_name, kur.code);
            currencyMessage(kur.selling, kur.buying, kur.full_name, kur.code);
        });

        // Print the data

    });
}

const currencyNames = ['USD', 'EUR'];

currencyNames.forEach(element => {
    getCurrencyRate(element);
});


